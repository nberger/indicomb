Indicomb
========

Here's the [link to the original twiki about indicomb][1]

Indicomb is a script to help generate a summary page of a subset of meetings on indico.

Example usage from command line (see section below about authentication for api token and secret keys)

```bash
./indicomb.py --output /afs/cern.ch/user/w/will/www/MyMeetings.html --categoryNumbers 3286 --includeList MyString AnotherString --startDate 2019-07-01 --API_KEY <insert token> --SECRET_KEY <insert secret>
```

Indicomb can be used either as a command-line tool, or as a function you call from a python script. See the command-line tool help for the various options. 

For use from a python script, just call the main ```indicomb``` method with the options you want (after importing):

```python
from indicomb import indicomb
indicomb( headerHTML="My Meetings",
          API_KEY= ... , SECRET_KEY= ... ,
          output="/afs/cern.ch/user/w/will/www/MyMeetings.html",
          startDate="2019-07-01",
          includeList=["MyString","AnotherString"],
          categoryNumbers=[3286])

```

*New:* ics calendar file will automatically be created by default. Set the ```calendarName``` argument to ```None``` to disable. 

A bar will also be generated at the top of the page with links for adding the calendar. To disable this bar, use the ```noTopbar``` option.

Indicomb Arguments
-----------------------

| argument | description |
| ------ | ------ |
| API_TOKEN | Generate an API Token to use with indicomb by following instructions at https://indico.cern.ch/user/api/ ... you provide this argument *or* the API_KEY and SECRET_KEY, not both sets (API_KEY will soon be deprecated by indico) |
| API_KEY |	This is the token you get from https://indico.cern.ch/user/api/ you must provide this for security reasons (see section below) |
| SECRET_KEY |	This is the secret you get from https://indico.cern.ch/user/api/ you must provide this for security reasons |
| output |	desired location for the output html |
| categoryNumbers |	A list of indico categories that you wish to search meetings for. You can determine the indico category by finding one of your meetings of interest and clicking the 'up' arrow at the top of the page to take you to the category. Look at the URL of that page, which will contain the category number |
| includeList |	A list of strings used to identify selected meetings |
| startDate (default="2017-04-01") |	Earliest date from which to look for meetings |
| endDate (default="+14d") | Latest date up to which to look for meetings. The default will look up to 2 weeks into the future |
| excludeList (optional) |	A list of strings used to veto meetings |
| headerHTML (optional)	| A string of html that will appear at the top of the page |
| prebodyHTML (optional) |	A string of html that is inserted before the body ... useful for adding a custom style sheet (requested by M LeBlanc) |
| speakerList (optional) |	List of strings searched for in the contributor name or institute, to filter for only contributions from certain people or institutes (requested by C Macdonald) |
| indico_site (optional) |	If using this script with a site other than indico.cern.ch, you can specify the url of it here. Defaults to "http://indico.cern.ch" (requested by H Oide) |
| calendarName (optional) | generates ics file. Leave blank to use the output html filename to set name. Set to None to disable |
| noTopBar (default=False) | Add this flag if you want to disable to automatically generated top bar with the link to calendar subscriptions |

Scheduling indicomb in your acrontab
------------------------------------

To get your page to be updated on regular intervals, you will want to
add it to your `acrontab`. You can learn more about this from
[CERN's `acron` docs][2], but a quick example of if you wanted your
python script to run 3 times a day, at midday, 2pm, and 8pm (CERN
time), then you could do the following:

You need to add the top level script to your acrontab:

```bash
acrontab -e
```

will bring up the editor, then add this line:

```bash
0 12,14,20 * * * lxplus ~/path/to/your/indicomb_script.py
```

Or something similar that points to your script.

Authentication
--------------

You should pretend you care about security: this way when your account
gets hacked and CERN's secrets are revealed, you can say you did your
best!

Unfortunately this means this can't work "out of the box". You'll have
to [get indico credentials][3]. Then you can paste your keys into a
file called `~/.indico-secret-key`, which should have the following
format:

```
XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
YYYYYYYY-YYYY-YYYY-YYYY-YYYYYYYYYYYY
```

The first line is the token, second is the "secret".

Acknowledgements
----------------
Original author and major feature developer: Will Buttinger

Current maintainer: Dan Guest

Contributors:
  * Yu-Heng Chen - ics calendar files


[1]: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/Indicomb
[2]: http://information-technology.web.cern.ch/services/fe/afs/howto/authenticate-processes
[3]: https://indico.cern.ch/user/api/
